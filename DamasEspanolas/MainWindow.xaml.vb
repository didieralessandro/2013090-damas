﻿Class MainWindow
    Private _rectangulosTablero(64) As Rectangle
    Private _piezaMovida As Object
    Private _espacioOcupado(64) As Boolean
    Private _posA, _posN As Integer
    Private Turno As Integer = 1
    Private Comer As Integer
    Private _fichasBlancas(12) As FichaBlanca
    Private _fichasNegras(12) As FichaNegra
    Private _controladorTurno As Boolean
    Private _fichaComida As Object
    Private _numeroFicha As Integer
    Private _numeroFicha2 As Integer
    Private ladoComido As Boolean
    Private _x As Integer
    Private _cantidadNegra As Integer = 12
    Private _cantidadBlanca As Integer = 12
    Private _comido As Boolean
    Private CoronaBlanca(12) As CoronaBlanca
    Private contarpiezacoronaw As Integer = 0



    Private Sub CargarTablero()
        _cantidadBlanca = 12
        _cantidadNegra = 12
        Dim contador As Integer = 0
        Dim left As Integer = 0
        Dim top As Integer = 1

        For y As Integer = 0 To 7
            For x As Integer = 0 To 7
                Dim rect As New Rectangle
                rect.Width = 44
                rect.Height = 44
                _rectangulosTablero(contador) = rect

                If y Mod 2 = 0 Then
                    If x Mod 2 = 0 Then
                        rect.Fill = Brushes.White
                    Else
                        rect.Fill = Brushes.Black
                    End If
                Else
                    If x Mod 2 = 0 Then
                        rect.Fill = Brushes.Black
                    Else
                        rect.Fill = Brushes.White
                    End If
                End If
                Canvas.SetTop(rect, CDbl(top))
                Canvas.SetLeft(rect, CDbl(left))
                'AddHandler FondoRectangulo.MouseDown, AddressOf EventClickTablero
                Tablero.Children.Add(rect)
                contador += 1
                left += 44
            Next
            top += 44
            left = 0
        Next 'Crea cada cuadro para los espacios de las fichas
    End Sub
    Private Sub CargarFichasBlancas()
        Dim conta As Integer = 0
        Dim c As Integer = 1
        Dim b As Double = 44
        Do
            Dim fichaBlanca As New FichaBlanca 'Metodo para Primera fila de fichas blancas
            _fichasBlancas(conta) = fichaBlanca
            conta += 1
            Canvas.SetLeft(fichaBlanca, b)
            Canvas.SetTop(fichaBlanca, 221)
            'AddHandler _fichasBlancas(conta).MouseLeftButtonDown, AddressOf Toque
            Tablero.Children.Add(fichaBlanca)
            _espacioOcupado(b / 44 - 1) = True
            b += 88
            c += 1
        Loop While (c < 5)
        c = 1
        b = 0
        Do
            Dim fichaBlanca As New FichaBlanca 'Metodo para Segunda fila de fichas blancas
            _fichasBlancas(conta) = fichaBlanca
            conta += 1
            Canvas.SetLeft(fichaBlanca, b)
            Canvas.SetTop(fichaBlanca, 221 + 44)
            'AddHandler _fichasBlancas(conta).MouseDown, AddressOf Toque
            Tablero.Children.Add(fichaBlanca)
            _espacioOcupado((b / 44) + 9) = True
            b += 88
            c += 1
        Loop While (c <= 4)
        c = 1
        b = 44
        Do
            Dim fichaBlanca As New FichaBlanca 'Metodo para Tercera fila de fichas blancas
            _fichasBlancas(conta) = fichaBlanca
            conta += 1
            Canvas.SetLeft(fichaBlanca, b)
            Canvas.SetTop(fichaBlanca, 221 + 88)
            'AddHandler _fichasBlancas(conta).MouseDown, AddressOf Toque
            _espacioOcupado((b / 44) + 15) = True
            Tablero.Children.Add(fichaBlanca)
            b += 88
            c += 1
        Loop While (c <= 4)
    End Sub

    Private Sub CargarFichasNegras()
        Dim conta As Integer
        Dim c As Integer = 1
        Dim b As Double = 0
        Do
            Dim fichaNegra As New FichaNegra 'Metodo para Primera fila de fichas negras
            _fichasNegras(conta) = fichaNegra
            conta += 1
            Canvas.SetLeft(fichaNegra, b)
            Canvas.SetTop(fichaNegra, 88)
            'AddHandler _fichasNegras(conta).MouseDown, AddressOf Toque
            Tablero.Children.Add(fichaNegra)
            _espacioOcupado((b / 44 + 57)) = True
            b += 88
            c += 1
        Loop While (c <= 4)
        c = 1
        b = 44
        Do
            Dim fichaNegra As New FichaNegra 'Metodo para Segunda fila de fichas negras
            _fichasNegras(conta) = fichaNegra
            conta += 1
            Canvas.SetLeft(fichaNegra, b)
            Canvas.SetTop(fichaNegra, 88 - 44)
            ' AddHandler _fichasNegras(conta).MouseDown, AddressOf Toque
            _espacioOcupado((b / 44 + 47)) = True
            Tablero.Children.Add(fichaNegra)
            b += 88
            c += 1
        Loop While (c <= 4)
        c = 1
        b = 0
        Do
            Dim fichaNegra As New FichaNegra 'Metodo para Tercera fila de fichas negras
            _fichasNegras(conta) = fichaNegra
            conta += 1
            Canvas.SetLeft(fichaNegra, b)
            Canvas.SetTop(fichaNegra, 0)
            ' AddHandler _fichasNegras(conta).MouseDown, AddressOf Toque
            _espacioOcupado(b / 44 + 39) = True
            If b > 263 Then
                _espacioOcupado(351 / 44 + 39) = True
                _espacioOcupado(351 / 44 + 31) = False
            End If
            Tablero.Children.Add(fichaNegra)
            b += 88
            c += 1
        Loop While (c < 5)
    End Sub

    Public Sub Moverse(sender As Object, e As MouseEventArgs)
        Dim espacios(64) As Rectangle

    End Sub

    Private Sub IniciarPiezas(sender As Object, e As EventArgs) Handles Tablero.Initialized
        CargarTablero()
        CargarFichasBlancas()
        CargarFichasNegras()
        Turnos()
    End Sub 'Carga el tablero y las fichas en cada espacio

    Private Sub Turnos()
        If (IsNothing(_fichaComida)) Then
        Else
            If TypeOf _fichaComida Is FichaBlanca Then
                _cantidadBlanca -= 1
                MsgBox("El negro se ha comido " & 12 - _cantidadBlanca)
            Else
                _cantidadNegra -= 1
                MsgBox("El blanco ha comido " & 12 - _cantidadNegra)
            End If
            If VerGanador(_piezaMovida) Then
                MsgBox("Ganaste")
                CargarTablero()
                devolverEspacios()
                txtBlanco.Clear()
                txtNegro.Clear()
            End If
        End If

        If _controladorTurno Then
            _controladorTurno = False
            For x = 0 To 12
                Try
                    AddHandler _fichasNegras(x).MouseDown, AddressOf Toque
                    RemoveHandler _fichasBlancas(x).MouseDown, AddressOf Toque
                Catch ex As Exception

                End Try
            Next
        Else
            _controladorTurno = True
            Try
                For x = 0 To 12
                    RemoveHandler _fichasNegras(x).MouseDown, AddressOf Toque
                    AddHandler _fichasBlancas(x).MouseDown, AddressOf Toque
                Next

            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub devolverEspacios()
        For x = 0 To 63
            _espacioOcupado(x) = False
        Next
    End Sub
    Private Sub btnEmpate_Click(sender As Object, e As RoutedEventArgs) Handles btnEmpate.Click
        MsgBox("Juego Empatado")
        devolverEspacios()
        Tablero.Children.Clear()
        txtBlanco.Clear()
        txtNegro.Clear()
        MsgBox("El juego se volvera a iniciar", MsgBoxStyle.SystemModal)
        CargarTablero()
        CargarFichasBlancas()
        CargarFichasNegras()
        _controladorTurno = False
        Turnos()
    End Sub

    Private Sub btnTerminar_Click(sender As Object, e As RoutedEventArgs) Handles btnTerminar.Click
        CargarTablero()
        devolverEspacios()
        txtBlanco.Clear()
        txtNegro.Clear()
    End Sub

    Private Sub btnReiniciar_Click(sender As Object, e As RoutedEventArgs) Handles btnReiniciar.Click
        devolverEspacios()
        CargarTablero()
        CargarFichasBlancas()
        CargarFichasNegras()
        _controladorTurno = False
        Turnos()
    End Sub

    Private Presionado As Boolean
    Private Jugadoraso As Object

    Private Sub SaltarBlancaDrcha(sender As Object, x As Integer)
        _posA = x
        For x2 = 0 To 63
            If (Canvas.GetTop(sender) - 89 = Canvas.GetTop(Tablero.Children(x2)) Or Canvas.GetTop(sender) - 88 = Canvas.GetTop(Tablero.Children(x2))) And TypeOf sender Is FichaBlanca Then
                If Canvas.GetLeft(sender) + 88 = Canvas.GetLeft(Tablero.Children(x2)) Or Canvas.GetLeft(sender) + 89 = Canvas.GetLeft(Tablero.Children(x2)) Then
                    For cont = 0 To 11
                        If Canvas.GetTop(sender) - 44 = Canvas.GetTop(_fichasNegras(cont)) Or Canvas.GetTop(sender) - 45 = Canvas.GetTop(_fichasNegras(cont)) Then
                            If Canvas.GetLeft(sender) + 44 = Canvas.GetLeft(_fichasNegras(cont)) Or Canvas.GetLeft(sender) + 45 = Canvas.GetLeft(_fichasNegras(cont)) Then
                                If _espacioOcupado(x2) = False Then
                                    MsgBox("Debes comer")
                                    _rectangulosTablero(x2).Fill = Brushes.Blue
                                    AddHandler _rectangulosTablero(x2).MouseLeftButtonDown, AddressOf ComerFichaNegra
                                    _x = x2
                                    For cont3 = 0 To 11
                                        If Canvas.GetLeft(sender) + 44 = Canvas.GetLeft(_fichasNegras(cont3)) Or Canvas.GetLeft(sender) + 45 = Canvas.GetLeft(_fichasNegras(cont3)) Then
                                            If Canvas.GetTop(sender) - 44 = Canvas.GetTop(_fichasNegras(cont3)) Or Canvas.GetTop(sender) - 45 = Canvas.GetTop(_fichasNegras(cont3)) Then
                                                'Tablero.Children.Remove(_fichasNegras(cont3))
                                                _fichaComida = (_fichasNegras(cont3))
                                                _numeroFicha2 = cont3
                                                ladoComido = False
                                            End If
                                        End If
                                    Next
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        Next
        SaltarBlancaIzq(sender, x)
    End Sub

    Private Sub SaltarBlancaIzq(sender As Object, x As Integer)
        _posA = x
        For x2 = 0 To 63
            If (Canvas.GetTop(sender) - 89 = Canvas.GetTop(Tablero.Children(x2)) Or Canvas.GetTop(sender) - 88 = Canvas.GetTop(Tablero.Children(x2))) And TypeOf sender Is FichaBlanca Then
                If Canvas.GetLeft(sender) - 88 = Canvas.GetLeft(Tablero.Children(x2)) Or Canvas.GetLeft(sender) - 89 = Canvas.GetLeft(Tablero.Children(x2)) Then
                    Try
                        For cont = 0 To 11
                            If Canvas.GetTop(sender) - 44 = Canvas.GetTop(_fichasNegras(cont)) Or Canvas.GetTop(sender) - 45 = Canvas.GetTop(_fichasNegras(cont)) Then
                                If Canvas.GetLeft(sender) - 44 = Canvas.GetLeft(_fichasNegras(cont)) Or Canvas.GetLeft(sender) - 45 = Canvas.GetLeft(_fichasNegras(cont)) Then
                                    If _espacioOcupado(x2) = False Then
                                        MsgBox("Debes comer")
                                        _rectangulosTablero(x2).Fill = Brushes.Blue
                                        AddHandler _rectangulosTablero(x2).MouseLeftButtonDown, AddressOf ComerFichaNegra
                                        _x = x2
                                        For cont3 = 0 To 11
                                            If Canvas.GetLeft(sender) - 44 = Canvas.GetLeft(_fichasNegras(cont3)) Or Canvas.GetLeft(sender) - 45 = Canvas.GetLeft(_fichasNegras(cont3)) Then
                                                If Canvas.GetTop(sender) - 44 = Canvas.GetTop(_fichasNegras(cont3)) Or Canvas.GetTop(sender) - 45 = Canvas.GetTop(_fichasNegras(cont3)) Then
                                                    _fichaComida = (_fichasNegras(cont3))
                                                    _numeroFicha = cont3
                                                    ladoComido = True
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            End If
                        Next
                    Catch ex As Exception

                    End Try
                End If
            End If
        Next
    End Sub

    Private Sub SaltarNegraDrcha(sender As Object, x As Integer)
        _posA = x
        For x2 = 0 To 63
            If (Canvas.GetTop(sender) + 89 = Canvas.GetTop(Tablero.Children(x2)) Or Canvas.GetTop(sender) + 88 = Canvas.GetTop(Tablero.Children(x2))) And TypeOf sender Is FichaNegra Then
                If Canvas.GetLeft(sender) + 88 = Canvas.GetLeft(Tablero.Children(x2)) Or Canvas.GetLeft(sender) + 89 = Canvas.GetLeft(Tablero.Children(x2)) Then
                    For cont = 0 To 11
                        If Canvas.GetTop(sender) + 44 = Canvas.GetTop(_fichasBlancas(cont)) Or Canvas.GetTop(sender) + 45 = Canvas.GetTop(_fichasBlancas(cont)) Then
                            If Canvas.GetLeft(sender) + 44 = Canvas.GetLeft(_fichasBlancas(cont)) Or Canvas.GetLeft(sender) + 45 = Canvas.GetLeft(_fichasBlancas(cont)) Then
                                If _espacioOcupado(x2) = False Then
                                    MsgBox("Debes comer")
                                    _rectangulosTablero(x2).Fill = Brushes.Blue
                                    AddHandler _rectangulosTablero(x2).MouseLeftButtonDown, AddressOf ComerFichaBlanca
                                    _x = x2
                                    For cont3 = 0 To 11
                                        If Canvas.GetLeft(sender) + 44 = Canvas.GetLeft(_fichasBlancas(cont3)) Or Canvas.GetLeft(sender) + 45 = Canvas.GetLeft(_fichasBlancas(cont3)) Then
                                            If Canvas.GetTop(sender) + 44 = Canvas.GetTop(_fichasBlancas(cont3)) Or Canvas.GetTop(sender) + 45 = Canvas.GetTop(_fichasBlancas(cont3)) Then
                                                'Tablero.Children.Remove(_fichasNegras(cont3))
                                                _fichaComida = (_fichasBlancas(cont3))
                                                _numeroFicha2 = cont3
                                                ladoComido = False
                                            End If
                                        End If
                                    Next
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        Next
        SaltarNegraIzq(sender, x)
    End Sub

    Private Sub SaltarNegraIzq(sender As Object, x As Integer)
        _posA = x
        For x2 = 0 To 63
            If (Canvas.GetTop(sender) + 89 = Canvas.GetTop(Tablero.Children(x2)) Or Canvas.GetTop(sender) + 88 = Canvas.GetTop(Tablero.Children(x2))) And TypeOf sender Is FichaNegra Then

                If Canvas.GetLeft(sender) - 88 = Canvas.GetLeft(Tablero.Children(x2)) Or Canvas.GetLeft(sender) - 89 = Canvas.GetLeft(Tablero.Children(x2)) Then

                    Try
                        For cont = 0 To 11
                            If Canvas.GetTop(sender) + 44 = Canvas.GetTop(_fichasBlancas(cont)) Or Canvas.GetTop(sender) + 45 = Canvas.GetTop(_fichasBlancas(cont)) Then
                                If Canvas.GetLeft(sender) - 44 = Canvas.GetLeft(_fichasBlancas(cont)) Or Canvas.GetLeft(sender) - 45 = Canvas.GetLeft(_fichasBlancas(cont)) Then
                                    If _espacioOcupado(x2) = False Then
                                        MsgBox("Debes comer")
                                        _rectangulosTablero(x2).Fill = Brushes.Blue
                                        AddHandler _rectangulosTablero(x2).MouseLeftButtonDown, AddressOf ComerFichaBlanca
                                        _x = x2
                                        For cont3 = 0 To 11
                                            If Canvas.GetLeft(sender) - 44 = Canvas.GetLeft(_fichasBlancas(cont3)) Or Canvas.GetLeft(sender) - 45 = Canvas.GetLeft(_fichasBlancas(cont3)) Then
                                                If Canvas.GetTop(sender) + 44 = Canvas.GetTop(_fichasBlancas(cont3)) Or Canvas.GetTop(sender) + 45 = Canvas.GetTop(_fichasBlancas(cont3)) Then
                                                    _fichaComida = (_fichasBlancas(cont3))
                                                    _numeroFicha = cont3
                                                    ladoComido = True
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            End If
                        Next
                    Catch ex As Exception

                    End Try
                End If
            End If
        Next
    End Sub

    Private Sub ComerFichaBlanca(sender As Object, e As EventArgs)
        For x = 0 To 63
            If Canvas.GetTop(_piezaMovida) = Canvas.GetTop(_rectangulosTablero(x)) Or Canvas.GetTop(_piezaMovida) + 1 = Canvas.GetTop(_rectangulosTablero(x)) _
                 Then
                If Canvas.GetLeft(_piezaMovida) = Canvas.GetLeft(_rectangulosTablero(x)) Then
                    _posA = x
                End If
            End If
        Next
        Try
            If Canvas.GetLeft(sender) = Canvas.GetLeft(_rectangulosTablero(_x)) And Canvas.GetTop(sender) = Canvas.GetTop(_rectangulosTablero(_x)) Then
                For x = 0 To 63
                    If _numeroFicha > 0 Then
                        If Canvas.GetTop(_fichasBlancas(_numeroFicha)) = Canvas.GetTop(_rectangulosTablero(x)) Or Canvas.GetTop(_fichasBlancas(_numeroFicha)) + 1 = Canvas.GetTop(_rectangulosTablero(x)) Then
                            If Canvas.GetLeft(_fichasBlancas(_numeroFicha)) = Canvas.GetLeft(_rectangulosTablero(x)) Then
                                _espacioOcupado(x) = False
                            End If
                        End If
                    Else
                        If Canvas.GetTop(_fichasBlancas(_numeroFicha2)) = Canvas.GetTop(_rectangulosTablero(x)) Or Canvas.GetTop(_fichasBlancas(_numeroFicha2)) + 1 = Canvas.GetTop(_rectangulosTablero(x)) Then
                            If Canvas.GetLeft(_fichasBlancas(_numeroFicha2)) = Canvas.GetLeft(_rectangulosTablero(x)) Then
                                _espacioOcupado(x) = False
                            End If
                        End If
                    End If
                Next
                _espacioOcupado(_x) = True
                If _numeroFicha > 0 Then
                    Tablero.Children.Remove(_fichasBlancas(_numeroFicha))
                Else
                    Tablero.Children.Remove(_fichasBlancas(_numeroFicha2))
                End If
                Canvas.SetLeft(_piezaMovida, Canvas.GetLeft(_rectangulosTablero(_x)))
                Canvas.SetTop(_piezaMovida, Canvas.GetTop(_rectangulosTablero(_x)))
            End If
            If _comido = False Then
                _espacioOcupado(_posA) = False
                Clean()
                Turnos()
                _comido = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComerFichaNegra(sender As Object, e As EventArgs)
        'Tablero.Children.Remove(_fichasNegras(_numeroFicha2))
        For x = 0 To 63
            If Canvas.GetTop(_piezaMovida) = Canvas.GetTop(_rectangulosTablero(x)) Or Canvas.GetTop(_piezaMovida) + 1 = Canvas.GetTop(_rectangulosTablero(x)) _
                 Then
                If Canvas.GetLeft(_piezaMovida) = Canvas.GetLeft(_rectangulosTablero(x)) Then
                    _posA = x
                End If
            End If
        Next
        Try
            If Canvas.GetLeft(sender) = Canvas.GetLeft(_rectangulosTablero(_x)) And Canvas.GetTop(sender) = Canvas.GetTop(_rectangulosTablero(_x)) Then
                For x = 0 To 63
                    If _numeroFicha > 0 Then
                        If Canvas.GetTop(_fichasNegras(_numeroFicha)) = Canvas.GetTop(_rectangulosTablero(x)) Or Canvas.GetTop(_fichasNegras(_numeroFicha)) + 1 = Canvas.GetTop(_rectangulosTablero(x)) Then
                            If Canvas.GetLeft(_fichasNegras(_numeroFicha)) = Canvas.GetLeft(_rectangulosTablero(x)) Then
                                _espacioOcupado(x) = False
                            End If
                        End If
                    Else
                        If Canvas.GetTop(_fichasNegras(_numeroFicha2)) = Canvas.GetTop(_rectangulosTablero(x)) Or Canvas.GetTop(_fichasNegras(_numeroFicha2)) + 1 = Canvas.GetTop(_rectangulosTablero(x)) Then
                            If Canvas.GetLeft(_fichasNegras(_numeroFicha2)) = Canvas.GetLeft(_rectangulosTablero(x)) Then
                                _espacioOcupado(x) = False
                            End If
                        End If
                    End If
                Next
                _espacioOcupado(_x) = True
                If _numeroFicha > 0 Then
                    Tablero.Children.Remove(_fichasNegras(_numeroFicha))
                Else
                    Tablero.Children.Remove(_fichasNegras(_numeroFicha2))
                End If
                '_espacioOcupado(posFicha) = False
                Canvas.SetLeft(_piezaMovida, Canvas.GetLeft(_rectangulosTablero(_x)))
                Canvas.SetTop(_piezaMovida, Canvas.GetTop(_rectangulosTablero(_x)))
            End If
            If _comido = False Then
                _espacioOcupado(_posA) = False
                Clean()
                Turnos()
                _comido = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function VerGanador(sender As Object) As Boolean
        If TypeOf (sender) Is FichaBlanca Then
            If _cantidadNegra = 0 Then
                Return True
            End If
        Else
            If _cantidadBlanca = 0 Then
                Return True
            End If
        End If

        Return False
    End Function

    Private Sub Espacios(sender As Object)
        _comido = False
        _numeroFicha = 0
        _numeroFicha2 = 0
        Clean()
        'MsgBox(Canvas.GetTop(sender) & " " & Canvas.GetLeft(sender))
        'MsgBox(Canvas.GetTop(sender)+44-Canvas.GetTop(Tablero.Children(x)))
        For x As Integer = 0 To 63
            'MsgBox(Canvas.GetTop(sender) + 45 - Canvas.GetTop(Tablero.Children(x)))
            '_rectangulosTablero(x).Fill = Brushes.Green
            If Canvas.GetLeft(sender) - 44 = Canvas.GetLeft(Tablero.Children(x)) Or Canvas.GetLeft(sender) + 44 = Canvas.GetLeft(Tablero.Children(x)) Then
                If Canvas.GetTop(sender) - 44 = Canvas.GetTop(Tablero.Children(x)) And TypeOf sender Is FichaBlanca Then
                    If _espacioOcupado(x) = False Then
                        _rectangulosTablero(x).Fill = Brushes.Blue
                        _posN = x
                        _posA = x
                        AddHandler _rectangulosTablero(x).MouseLeftButtonDown, AddressOf Movida
                    End If
                    If TypeOf (sender) Is FichaBlanca Then
                        SaltarBlancaDrcha(sender, x)
                    Else
                        SaltarNegraDrcha(sender, x)
                    End If

                    'DirectCast(Tablero.Children(x), Rectangle).Stroke = Brushes.Green
                ElseIf (Canvas.GetTop(sender) + 45 = Canvas.GetTop(Tablero.Children(x)) Or Canvas.GetTop(sender) + 44 = Canvas.GetTop(Tablero.Children(x))) And TypeOf sender Is FichaNegra Then
                    If _espacioOcupado(x) = False Then
                        _rectangulosTablero(x).Fill = Brushes.Blue
                        _posN = x
                        _posA = x
                        AddHandler _rectangulosTablero(x).MouseLeftButtonDown, AddressOf Movida
                    End If
                    If TypeOf (sender) Is FichaBlanca Then
                        SaltarBlancaDrcha(sender, x)
                    Else
                        SaltarNegraDrcha(sender, x)
                    End If
                End If
                _piezaMovida = sender
            End If
        Next
    End Sub

    Private Sub Movida(sender As Object, e As EventArgs)
        _fichaComida = Nothing
        For x = 0 To 63
            'MsgBox(Canvas.GetTop(_piezaMovida) & " " & Canvas.GetTop(_rectangulosTablero(x)))
            '_rectangulosTablero(x).Fill = Brushes.Azure
            If Canvas.GetTop(_piezaMovida) = Canvas.GetTop(_rectangulosTablero(x)) Or Canvas.GetTop(_piezaMovida) + 1 = Canvas.GetTop(_rectangulosTablero(x)) _
                 Then
                If Canvas.GetLeft(_piezaMovida) = Canvas.GetLeft(_rectangulosTablero(x)) Then
                    'MsgBox("hola")
                    '_rectangulosTablero(_posA).Fill = Brushes.Yellow
                    _posA = x
                End If
            End If
            If Canvas.GetTop(_rectangulosTablero(x)) = Canvas.GetTop(sender) Then
                If Canvas.GetLeft(sender) = Canvas.GetLeft(_rectangulosTablero(x)) Then
                    'MsgBox("holadafkdajsflkdasjfkldasj")
                    _posN = x
                End If
            End If
        Next
        Clean()
        Canvas.SetTop(_piezaMovida, Canvas.GetTop(sender))
        Canvas.SetLeft(_piezaMovida, Canvas.GetLeft(sender))
        _espacioOcupado(_posA) = False
        _espacioOcupado(_posN) = True
        'ColorFicha()
        CoronarW(Canvas.GetTop(_piezaMovida), Canvas.GetLeft(_piezaMovida), _piezaMovida)
        Tablero.Children.Remove(_piezaMovida)
        Tablero.Children.Add(_piezaMovida)
        Turnos()
    End Sub

    Private Sub ColorFicha()
        For x = 0 To 63
            If _espacioOcupado(x) = True Then
                _rectangulosTablero(x).Fill = Brushes.Green
            End If
        Next
    End Sub

    Private Sub Toque(sender As Object, e As EventArgs)
        Clean()
        Jugadoraso = sender
        Presionado = True
        Espacios(sender)
    End Sub

    Private Sub Clean()
        Dim contador As Integer = 0
        Dim left As Integer = 0
        Dim top As Integer = 1

        For y As Integer = 0 To 7
            For x As Integer = 0 To 7
                If y Mod 2 = 0 Then
                    If x Mod 2 = 0 Then
                        _rectangulosTablero(contador).Fill = Brushes.White
                    Else
                        _rectangulosTablero(contador).Fill = Brushes.Black
                    End If
                Else
                    If x Mod 2 = 0 Then
                        _rectangulosTablero(contador).Fill = Brushes.Black
                    Else
                        _rectangulosTablero(contador).Fill = Brushes.White
                    End If
                End If
                RemoveHandler (_rectangulosTablero(contador)).MouseLeftButtonDown, AddressOf Movida
                contador += 1
                left += 44
            Next
            top += 44
            left = 0
        Next 'Crea cada cuadro para los espacios de las fichas
    End Sub

    Private Sub EventClickTablero(sender As Object, e As EventArgs)
        If Presionado Then
            If (True) Then

                Canvas.SetTop(Jugadoraso, Canvas.GetTop(sender))
                Canvas.SetLeft(Jugadoraso, Canvas.GetLeft(sender))
            End If
        End If
    End Sub

    Private Sub CoronarW(Top As Integer, Left As Integer, piezaCoronada As Object)
        If (Top = 1) And (Left = 0 Or Left = 88 Or Left = 176 Or Left = 264) Then
            Dim coronaWh As New CoronaBlanca
            Canvas.SetTop(coronaWh, Top)
            Canvas.SetLeft(coronaWh, Left)
            Tablero.Children.Remove(piezaCoronada)
            Tablero.Children.Add(coronaWh)
            CoronaBlanca(contarpiezacoronaw) = coronaWh
            contarpiezacoronaw += 1
        End If
    End Sub

End Class
